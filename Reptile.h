/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Reptile.h
 * Author: Ana
 *
 * Created on 14 de Junho de 2019, 10:29
 */

#ifndef REPTILE_H
#define REPTILE_H
#include "Animal.h"
#include <string>

class Reptile: public Animal {
    unsigned n_eggs;
public:
    Reptile(const std::string & s, const std::string & n, 
            int w, int a, int ne);
    void feed(int g) override;
    std::string toString() const override;
    virtual ~Reptile();
private:

};

#endif /* REPTILE_H */

