/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Animal.cpp
 * Author: Ana
 * 
 * Created on 14 de Junho de 2019, 10:29
 */

#include "Animal.h"
#include <sstream>
#include <iostream>
using namespace std;
Animal::Animal(const string & s, const string & n, 
            int w, int a){
    if(s != "")
        specie = s;
    else 
        specie = "undefined";
    if(n != "")
        name = n;
    else 
        name = "anonymous";
    if(w > 0)
        weight = w;
    else
        weight = 1;
    if (a >= 0)
        age = a;
    else            
        age = 0;
    cout << "Constructing Animal" << name <<endl;
}
std::string Animal::toString() const{
    ostringstream os;
    os << "Animal:" << name << '\t' << specie << "\tage:" <<
            age << "(months)\tweight:" << weight << "gr\n";
    return os.str();
}
void Animal::feed(int g){
    if(g >= 0)
        weight += g;
}
Animal::~Animal() {
    cout << "Destroying Animal" << name <<endl;
}

std::string Animal::getName() const {
    return name;
}

unsigned int Animal::getAge() const {
    return age;
}

void Animal::addWeight(unsigned int weight) {
    this->weight += weight;
}

unsigned int Animal::getWeight() const {
    return weight;
}

