/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//main.cpp
#include <iostream>
#include <vector>
#include "Animal.h"
#include "Reptile.h"
#include "Mammal.h"
using namespace std;

int main(){

//    Animal *lizz = new Reptile("Lizzard", "Tom", 100,8,3);
//    Animal *dog = new Mammal("Dog", "Bobby", 1500, 3,1);
//    Animal *cat = new Mammal("Cat", "Tareco", 800, 5, 1);   
    vector<Animal *> animals = {
        new Reptile("Lizzard", "Tom", 100,8,3),
         new Mammal("Dog", "Bobby", 1500, 3,1),
         new Mammal("Cat", "Tareco", 800, 5, 1)};
    
    for(int i=0; i < animals.size(); i++)
        cout << animals[i]->toString() << endl;
//    cout << lizz->toString() << endl;
//    cout << dog->toString() << endl;
//    cout << cat->toString() << endl;

    for(int i=0; i < animals.size(); i++)
        animals[i]->feed(100);
//    dog->feed(100);
//    lizz->feed(100);

//    cout << dog->toString() << endl;
//    cout << lizz->toString() << endl;

    for(int i=0; i < animals.size(); i++)
        cout << animals[i]->toString() << endl;
//  
//    delete lizz;
//    delete dog;
//    delete cat;
    for(int i=0; i < animals.size(); i++)
        delete animals[i];
    return 0;

}
