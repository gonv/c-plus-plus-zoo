/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Mammal.cpp
 * Author: Ana
 * 
 * Created on 14 de Junho de 2019, 10:30
 */

#include "Mammal.h"
#include <iostream>
#include <sstream>
using namespace std;

 Mammal::Mammal(const string & s, const string & n, 
            int w, int a, int nm):Animal(s,n,w,a){
     if(nm > 0)
         n_months = nm;
     else 
         n_months = 1;
     cout << "Constructing Mammal" << getName() <<endl;
 } 

 string Mammal::toString() const{
     ostringstream os;
     os << Animal::toString();
     os << "N months:" << n_months << endl;
     return os.str();
 }
 void Mammal::feed(int g){
     int increase=0;
     if(getAge() <= 12)
         increase = g/100;
     else
         if(g >= 0.05 * getWeight())
             increase = 10;
     addWeight(increase);
 }

Mammal::~Mammal() {
    cout << "Destroying Mammal" << getName() <<endl;
}

