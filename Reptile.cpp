/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Reptile.cpp
 * Author: Ana
 * 
 * Created on 14 de Junho de 2019, 10:29
 */

#include "Reptile.h"
#include <iostream>
#include <sstream>
using namespace std;
Reptile:: Reptile(const string & s, const string & n, 
            int w, int a, int ne):Animal(s,n,w,a){
     if(ne > 0)
         n_eggs = ne;
     else 
         n_eggs = 1;
     cout << "Constructing Reptile" << getName() <<endl;
 } 
string Reptile::toString() const{
     ostringstream os;
     os << Animal::toString();
     os << "N eggs:" << n_eggs << endl;
     return os.str();
 }

Reptile::~Reptile() {
       cout << "Destroying Reptile" << getName() <<endl;
}
void Reptile::feed(int g){
    int increase = 0;
    if(n_eggs > 10)
        increase = 2 * g / 100;
    else
        increase = 3 * g / 100;
    addWeight(increase);
}