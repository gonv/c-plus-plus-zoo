/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Animal.h
 * Author: Ana
 *
 * Created on 14 de Junho de 2019, 10:29
 */

#ifndef ANIMAL_H
#define ANIMAL_H
#include <string>

class Animal {
    std::string specie;
    std::string name;
    unsigned int weight, age;
 
public:
    Animal(const std::string & s, const std::string & n, 
            int w, int a=0);    
    std::string toString() const;
    virtual void feed(int g); //each derived behaves in a different way
    
    virtual ~Animal();
    virtual std::string getName() const;
    unsigned int getAge() const;
    void addWeight(unsigned int weight);
    unsigned int getWeight() const;
private:

};

#endif /* ANIMAL_H */

