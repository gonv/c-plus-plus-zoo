/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Mammal.h
 * Author: Ana
 *
 * Created on 14 de Junho de 2019, 10:30
 */

#ifndef MAMMAL_H
#define MAMMAL_H
#include "Animal.h"

class Mammal :public Animal{
    unsigned n_months;
public:
    Mammal(const std::string & s, const std::string & n, 
            int w, int a, int nm);  
    void feed(int g) override;
    std::string toString() const override;
    virtual ~Mammal();
private:

};

#endif /* MAMMAL_H */

